package ee.sda.hibernate.cinema;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "rooms", schema = "cinema")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer roomId;
    private Integer number;
    private Integer maxSeats;
    private String location;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<Seat> seats;


}
