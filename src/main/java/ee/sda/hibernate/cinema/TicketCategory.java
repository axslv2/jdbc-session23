package ee.sda.hibernate.cinema;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "ticket_categories", schema = "cinema")
public class TicketCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ticketCategoryId;
    private String type;
    private Integer price;
    @OneToMany(mappedBy = "category")
    private List<Ticket> tickets = new ArrayList<>();
}
