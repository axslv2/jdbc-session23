package ee.sda.hibernate.cinema;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "reservations", schema = "cinema")
@NoArgsConstructor
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer reservationId;
    private Integer isPaid; //in MySQL true/false is replaced by 1/0

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scheduleId")
    private Schedule schedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientId")
    private Client client;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "reservationSeat",
            joinColumns = @JoinColumn(name = "reservationId", referencedColumnName = "reservationId"),
            inverseJoinColumns = @JoinColumn(name = "seatId", referencedColumnName = "seatId"))
    List<Seat> seats = new ArrayList<>();
}
