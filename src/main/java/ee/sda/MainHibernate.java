package ee.sda;

import ee.sda.hibernate.hr.HibernateUtils;
import org.hibernate.Session;

public class MainHibernate {

    public static void main(String[] args) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        session.close();
    }
}